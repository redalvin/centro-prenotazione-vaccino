<?php
class Admin extends Model{
    public function check($username, $password){
        $passHash = md5($password);
        $sql = "SELECT * FROM users WHERE nome = ? AND password = ?";
        $req = Database::getBdd()->prepare($sql);
        $req->bind_param("ss", $username, $passHash);
        $req->execute();
        return  $req->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    public function showAllCentri()
    {
        $sql = "SELECT id, centri_vaccinali.name as centroName, centri_vaccinali.address as centroIndirizzo FROM centri_vaccinali";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    public function showAllSlots()
    {
        $sql = "SELECT slot_vaccinali.id as id, centri_vaccinali.name as nomeCentro, slot_vaccinali.dataTime as dataTime, isBusy FROM slot_vaccinali LEFT JOIN centri_vaccinali ON slot_vaccinali.idCentro=centri_vaccinali.id";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    public function showAllPrenotazioni()
    {
        $sql = "SELECT prenotazioni.id as id, users.nome as nome, users.cognome as cognome, centri_vaccinali.name as nomeCentro, slot_vaccinali.dataTime as dataTime FROM prenotazioni LEFT JOIN users ON users.id=prenotazioni.idUtente LEFT JOIN slot_vaccinali ON slot_vaccinali.id=prenotazioni.idSlot LEFT JOIN centri_vaccinali ON slot_vaccinali.idCentro=centri_vaccinali.id";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    public function showAllUsers()
    {
        $sql = "SELECT users.id as id, users.nome as nome, users.cognome as cognome, users.codicefiscale as codicefiscale, users.email as email FROM users WHERE users.isAdmin=0";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->get_result()->fetch_all(MYSQLI_ASSOC);
    }
}
