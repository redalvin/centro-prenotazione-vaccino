<?php
class Utente extends Model{

    public function create($codicefiscale, $nome, $cognome, $email, $idprenotazione){
        $sql = "INSERT INTO Utente (CodiceFiscale, Nome, Cognome, Email, idPrenotazione) VALUES (?, ?, ?, ?, ?)";
        $req = Database::getBdd()->prepare($sql);
        $req->bind_param("ssssi", $codicefiscale, $nome, $cognome, $email, $idprenotazione);
        return $req->execute();
    }

    public function showTask($codicefiscale){
        $sql = "SELECT * FROM Utente WHERE CodiceFiscale =".$codicefiscale;
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    public function showAllTasks(){
        $sql = "SELECT * FROM Utente";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    /* public function edit($id, $title, $description){
        $sql = "UPDATE tasks SET title = ?, description = ?, updated_at = ? WHERE id = ?";
        $req = Database::getBdd()->prepare($sql);
        $req->bind_param("sssi", $title, $description, date('Y-m-d H:i:s'), $id);
        return $req->execute();
    }

    public function delete($id){
        $sql = 'DELETE FROM tasks WHERE id = ?';
        $req = Database::getBdd()->prepare($sql);
        $req->bind_param("i", $id);

        if ($req->execute()){
            require(ROOT . "Models/Subtask.php");
            $subtask = new Subtask();
            return $subtask->deleteAll($id);
        }
        return false;
    } */
}
