<?php
class adminController extends Controller{
    
    function index(){
        require(ROOT . 'Models/Admin.php');
        $admin = new Admin();
        $this->render("index");
    }

    function login(){
        if (isset($_POST["username"]) && isset($_POST["password"])) {
            require(ROOT . 'Models/Admin.php');
            $user = new Admin();
            $result = $user->check($_POST["username"], $_POST["password"]) && $_POST["username"];
            if($result != null && $_POST["password"] != null){
                $_SESSION["user"] = $result;
                header("Location: ".WEBROOT."admin/index/");
            } else {
                $_SESSION["errorMessage"] = "Username o password non corretti";
                header("Location: ".WEBROOT."admin/login/");
            }
        }

        $this->render("login");
    }

    function viewcentri()
    {
        require(ROOT . 'Models/Admin.php');
        $centri = new Admin();
        $d['centri'] = $centri->showAllCentri();
        $this->set($d);
        $this->render("viewcentri");
    }

    function viewslots()
    {
        require(ROOT . 'Models/Admin.php');
        $slots = new Admin();
        $d['slots'] = $slots->showAllSlots();
        $this->set($d);
        $this->render("viewslots");
    }

    function viewprenotazioni()
    {
        require(ROOT . 'Models/Admin.php');
        $prenotazioni = new Admin();
        $d['prenotazioni'] = $prenotazioni->showAllPrenotazioni();
        $this->set($d);
        $this->render("viewprenotazioni");
    }

    function viewusers()
    {
        require(ROOT . 'Models/Admin.php');
        $users = new Admin();
        $d['users'] = $users->showAllUsers();
        $this->set($d);
        $this->render("viewusers");
    }
}
?>