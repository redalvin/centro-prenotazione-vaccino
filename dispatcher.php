<?php

class Dispatcher{

    private $request;

    public function dispatch(){
        $this->request = new Request();
        Router::parse($this->request->url, $this->request);

        $controller = $this->loadController();

        call_user_func_array([$controller, $this->request->action], $this->request->params);
    }

    public function loadController(){
        $name = $this->request->controller."Controller";
        if($name == "adminController" && $_SERVER['REQUEST_URI'] != "/PortaleWeb/admin/login/"){
            if(!$_SESSION["user"]){
                header("Location: ".WEBROOT."admin/login/");
                die("0");
            }
        }
        $file = ROOT.'Controllers/'.$name.'.php';
        require($file);
        $controller = new $name();
        return $controller;
    }

}
?>