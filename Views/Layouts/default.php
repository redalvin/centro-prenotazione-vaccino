<!doctype html>
<head>
    <meta charset="utf-8">

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" rel="stylesheet">

    <link href="starter-template.css" rel="stylesheet">

    <link href="https://www.regione.veneto.it/image/company_logo?img_id=402855&t=1616585507080" rel="icon">

    <style>
        body {
            padding-top: 5rem;
        }
        .starter-template {
            padding: 3rem 1.5rem;
            text-align: center;
        }
        .navbar-custom {
            background-color: white;
        }
    </style>
</head>

<body>
<nav class="navbar navbar-expand-md navbar-dark bg-primary fixed-top">
      <a class="navbar-brand" href="https://www.aulss3.veneto.it/index.cfm"><img style="max-width:100px; position:relative" src="https://www.regione.veneto.it/image/company_logo?img_id=402855&t=1616585507080"></img></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href='/PortaleWeb/home/index/'>
              <img style="max-width: 20px; margin-top: -4px; position:relative" src="https://www.ctapomezia.it/images/homew.png">
              Home <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href='/PortaleWeb/prenotazione/index/'>Prenota il vaccino</a>
          </li>
        </ul>
        <ul class="navbar-nav">
          <li class="nav-item active navbar-right">
            <a class="nav-link" href="/PortaleWeb/admin/login/">Amministrazione</a>
          </li>
        </ul>
      </div>
    </nav>


<main role="main" class="container">

    <div class="starter-template">

        <?php
        echo $content_for_layout;
        ?>

    </div>

</main>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

</body>
</html>

<style>
.footer {
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  background-color: #202020;
  color: #505050;
  text-align: center;
}
</style>

<div class = footer>
    Copyright @ 2021 ULSS 39. All rights reserved
</div>