<head>
    <title>Centro Prenotazione Vaccino - Prenotazioni</title>
</head>

<style>
    .container {
        padding-right: 100px;
        padding-left: 100px;
    }

    .form-group {
        text-align: left;
    }
</style>

<body>
<div class="row col-md-12 centered">
    <table class="table table-striped custab">
        <thead>
            <tr>
                <th>ID Prenotazione</th>
                <th>Nome E Cognome</th>
                <th>Nome centro</th>
                <th>Data</th>
            </tr>
        </thead>
        <?php
        foreach ($prenotazioni as $prenotazione) {
            echo '<tr>';
            echo "<td>" . $prenotazione['id'] . "</td>";
            echo "<td>" . $prenotazione['nome'] . " " . $prenotazione['cognome'] . "</td>";
            echo "<td>" . $prenotazione['nomeCentro'] . "</td>";
            echo "<td>" . $prenotazione['dataTime'] . "</td>";
            echo "</tr>";
        }
        ?>
    </table>
</div>

</body>
