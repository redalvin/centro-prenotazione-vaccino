<head>
    <title>Centro Prenotazione Vaccino - Utenti registrati</title>
</head>

<style>
    .container {
        padding-right: 100px;
        padding-left: 100px;
    }

    .form-group {
        text-align: left;
    }
</style>

<body>
<div class="row col-md-12 centered">
    <table class="table table-striped custab">
        <thead>
            <tr>
                <th>ID Utente</th>
                <th>Nome</th>
                <th>Cognome</th>
                <th>Codice Fiscale</th>
                <th>Email</th>
            </tr>
        </thead>
        <?php
        foreach ($users as $user) {
            echo '<tr>';
            echo "<td>" . $user['id'] . "</td>";
            echo "<td>" . $user['nome'] . "</td>";
            echo "<td>" . $user['cognome'] . "</td>";
            echo "<td>" . $user['codicefiscale'] . "</td>";
            echo "<td>" . $user['email'] . "</td>";
            echo "</tr>";
        }
        ?>
    </table>
</div>

</body>
