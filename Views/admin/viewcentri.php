<head>
    <title>Centro Prenotazione Vaccino - Centri Vaccinali</title>
</head>

<style>
    .container {
        padding-right: 100px;
        padding-left: 100px;
    }

    .form-group {
        text-align: left;
    }
</style>

<body>
<div class="row col-md-12 centered">
    <table class="table table-striped custab">
        <thead>
            <tr>
                <th>ID Centro</th>
                <th>Nome centro</th>
                <th>Indirizzo</th>
            </tr>
        </thead>
        <?php
        foreach ($centri as $centro) {
            echo '<tr>';
            echo "<td>" . $centro['id'] . "</td>";
            echo "<td>" . $centro['centroName'] . "</td>";
            echo "<td>" . $centro['centroIndirizzo'] . "</td>";
            echo "</tr>";
        }
        ?>
    </table>
</div>

</body>
