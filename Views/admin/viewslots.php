<head>
    <title>Centro Prenotazione Vaccino - Slot Esistenti</title>
</head>

<style>
    .container {
        padding-right: 100px;
        padding-left: 100px;
    }

    .form-group {
        text-align: left;
    }
</style>

<body>
<div class="row col-md-12 centered">
    <table class="table table-striped custab">
        <thead>
            <tr>
                <th>ID Slot</th>
                <th>Nome centro</th>
                <th>Data</th>
                <th>È prenotato</th>
            </tr>
        </thead>
        <?php
        foreach ($slots as $slot) {
            echo '<tr>';
            echo "<td>" . $slot['id'] . "</td>";
            echo "<td>" . $slot['nomeCentro'] . "</td>";
            echo "<td>" . $slot['dataTime'] . "</td>";
            if ($slot['isBusy'] == 0){
                echo "<td>Slot libero</td>";
            }else{
                echo "<td>Slot occupato</td>";
            }
            echo "</tr>";
        }
        ?>
    </table>
</div>

</body>
