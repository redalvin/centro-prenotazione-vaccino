<head>
    <title>Centro Prenotazione Vaccino - Amministrazione</title>
</head>

<h1>Gestione Amministrazione</h1><br><br>


<div style="float: left;">
    <a href="/PortaleWeb/admin/inseriscislot/" class="btn btn-primary btn-xs pull-right">Inserisci slot vaccinale<b> +</b></a>
</div>
<div style="float: right;">
    <a href="/PortaleWeb/admin/inseriscicentro/" class="btn btn-primary btn-xs pull-right">Inserisci centro vaccinale<b> +</b></a>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<div style="float: left;">
    <a href="/PortaleWeb/admin/viewprenotazioni" class="btn btn-primary btn-xs pull-right">Vedi prenotazioni</a>
</div>
<div style="float: right;">
    <a href="/PortaleWeb/admin/viewcentri" class="btn btn-primary btn-xs pull-right">Vedi centri vaccinali</a>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<div style="float: left;">
    <a href="/PortaleWeb/admin/viewslots" class="btn btn-primary btn-xs pull-right">Vedi slot vaccinali</a>
</div>
<div style="float: right;">
    <a href="/PortaleWeb/admin/viewusers" class="btn btn-primary btn-xs pull-right">Vedi utenti</a>
</div>


<style>
.footer {
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  background-color: #202020;
  color: #505050;
  text-align: center;
}
</style>

<div class = footer>
Copyright @ 2021 ULSS 39. All rights reserved
</div>
