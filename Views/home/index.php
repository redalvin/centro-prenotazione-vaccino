<head>
    <title>Centro Prenotazione Vaccino</title>
</head>

<h1>Benvenuto al centro prenotazione vaccini</h1><br><br>


<div>
    <a href="/PortaleWeb/prenotazione/index/" class="btn btn-primary btn-xs pull-right">Nuova prenotazione <b>+</b></a>
</div>

<br>
<div class="alert alert-warning"><p><strong>ATTENZIONE CALENDARI APERTI PER LE CLASSI DI ETA</strong>’ <strong>OLTRE I 60 ANNI E VULNERABILI</strong></p>  
<p>Da <strong>giovedì 29 aprile</strong>, per i cittadini dai 60 ai 79 anni è possibile prenotare l’appuntamento per la vaccinazione dal proprio <strong>Medico di Medicina Generale</strong> che aderisce al sistema della prenotazione ON LINE.<br /> Qualora il calendario del proprio medico non sia visibile è sempre possibile contattare il suo ambulatorio per sapere se la vaccinazione può essere eseguita e in che orari. I calendari attualmente non visibili potranno essere disponibili qualora il medico aderisca al sistema di prenotazione online</p>  <p>Per ulteriori informazioni <a href="https://www.aulss3.veneto.it/Vaccino-COVID-19">clicca qui</a> </p>  <p>Campagne vaccinali attualmente attive:</p>  <ul>  
  <li><strong>ultraquarantenni - calendari aperti </strong></li>  <li><strong>ultracinquantenni - calendari aperti </strong></li><li><strong>ultrasessantenni - calendari aperti </strong></li>  <li><strong>ultrasettantenni - calendari aperti </strong></li>  <li><strong>ultraottantenni - Libero accesso presso qualsiasi punto vaccinale</strong> CONSULTA GLI ORARI DELLE SEDI <a href="https://www.aulss3.veneto.it/Le-sedi-vaccinali-dell-ULSS-3-Serenissima">CLICCA QUI</a></li>  <li><strong>soggetti fragili - calendari aperti</strong></li>  <li><strong>disabili gravi (L. 104 art. 3 c.3) - calendari aperti</strong></li>  <li><strong>conviventi di persone immunodepresse gravi - con autocertificazione si veda sito ulss 3</strong></li>  <li><strong>conviventi/caregiver dei soggetti disabili gravi  - con autocertificazione si veda sito ulss 3</strong></li> </ul></div> 

<style>
.footer {
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  background-color: #202020;
  color: #505050;
  text-align: center;
}
</style>

<div class = footer>
Copyright @ 2021 ULSS 39. All rights reserved
</div>
