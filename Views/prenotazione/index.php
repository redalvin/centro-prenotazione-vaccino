<?php
    $nameErr = $$cognomeErr = $emailErr = "";

    if(empty($_POST['name'])){
        $nameeErr = "Campo obbligatorio";
    }

    if(empty($_POST['codicefiscale'])){
        $nameeErr = "Campo obbligatorio";
    }

    if(empty($_POST['cognome'])){
        $$cognomeErr = "Campo obbligatorio";
    }

    if(empty($_POST['email'])){
        $emailErr = "Campo obbligatorio*";
    } elseif(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
        $emailErr = "Formato email non valido.";
    }
?>

<!----------------------------------------------------->

<head>
    <title>Centro Prenotazione Vaccino - Nuova Prenotazione</title>
</head>

<h1>Nuova prenotazione</h1>

<i><strong><p style="color:red">*Campo obbligatorio.</p></strong></i>
<form method='post' action='#'>
<div class="form-group">
        <label for="nome">Codice Fiscale<strong style="color:red">*</strong></label>
        <input type="text" class="form-control" id="codicefiscale" placeholder="Inserisci un codice fiscale..." name="codicefiscale">
    </div>
    <div class="form-group">
        <label for="nome">Nome<strong style="color:red">*</strong></label>
        <input type="text" class="form-control" id="nome" placeholder="Inserisci un nome..." name="nome">
    </div>

    <div class="form-group">
        <label for="cognome">Cognome<strong style="color:red">*</strong></label>
        <input type="text" class="form-control" id="cognome" placeholder="Inserisci un cognome..." name="cognome">
    </div>

    <div class="form-group">
        <label for="email">Email<strong style="color:red">*</strong></label>
        <input type="text" class="form-control" id="email" placeholder="Inserisci un'email'..." name="email">
    </div>

    <div class="form-group">
        <label for="period">Slot disponibili<strong style="color:red">*</strong></label>
        <div class="form-group row">
            <div class="col-6 col-form-label">
                <label for="date1">Data di vaccinazione:</label>
                <input type="date" class="form-control" id="date1" placeholder="gg/mm/aaa">
            </div>
            <div class="col-6 col-form-label">
                <label for="date2">Luogo di vaccinazione:</label>
                <input type="date" class="form-control" id="date2" placeholder="gg/mm/aaa">
            </div>
        </div>
    </div>

    <div class="form-group">
        <input type="checkbox" required /> Acconsento al trattamento dati ai soli fini della prenotazione<strong style="color:red">*</strong> 
    </div>

    <a class='btn btn-danger btn-xs' href='/PortaleWeb/prenotazione/index/'><span class='glyphicon glyphicon-cancel'></span> Cancella</a>
    <button type="submit" class="btn btn-success">Fatto</button>
</form>

<style>
.footer {
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  background-color: #202020;
  color: #505050;
  text-align: center;
}
</style>

<div class = footer>
Copyright @ 2021 ULSS 39. All rights reserved
</div>

</body>













